package com.tdd.calculator;

public class Calculator {
    /**
     * Calculate sum of two value.
     *
     * @param x value
     * @param y value
     * @return int
     */
    public int sum(int x, int y){
        return x + y;
    }
}
