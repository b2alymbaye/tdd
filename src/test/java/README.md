#AAA Pattern
Tests typically follow the AAA pattern:

Arrange - Setup of the Scenario, e.g.: creating the input data
Act - Executing the action/method
Assert - Validation of the outcome
