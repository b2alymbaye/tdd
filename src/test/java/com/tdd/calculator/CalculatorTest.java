package com.tdd.calculator;


import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    private Calculator calculator;

    public CalculatorTest() {
        calculator = new Calculator();
    }

    @Test
    public void testSum_BothNumberArePositive_ShouldReturnPositiveNumber(){
//      Arrange
        int x = 5;
        int y = 18;

//      Act
        int result = this.calculator.sum(x, y);

//      Assert
        Assert.assertTrue(result > 0);
    }

    @Test
    public void testSum_BothNumberAreNegative_ShouldReturnNegativeNumber(){
//      Arrange
        int x = -2;
        int y = -6;

//      Act
        int result = this.calculator.sum(x, y);

//      Assert
        Assert.assertTrue(result < 0);
    }

    @Test
    public void testSum_NumbersAreOposite_ShouldReturnZero(){
//      Arrange
        int x = -4;
        int y = 4;

//      Act
        int result = this.calculator.sum(x, y);

//      Assert
        Assert.assertEquals(0, result);
    }
}
